In this problem the first obstacle to deal with is the fact that we can't hope to maintain the internal 
structure of a each label with $\mathcal{O}(k \log k)$ bits. However, a crucial property is that
once a join is performed, the vertices of a label become connected.
To solve this problem, we apply a technique called ``expectation from the outside'' in
\cite{Bui-XuanSTV13} that consists in guessing in advance what operation will happen next to a label
(this is not obvious from the expression because some joins will not add edges due to vertex
deletions) to design a bottom-up dynamic programming algorithm.

A state in our dynamic programming will consist of a near-partition $\mathcal{P}$ of labels according 
to the expected evolution of their connectivity, where two subsets have a special role : 
$P_\varnothing$ will denote labels that contain no vertex, $P_f$ will denote labels that will only
be joined to empty labels in the future, other subsets $P_1,\dots,P_k$ represent labels that may form a
connected component in the future, together they form $\mathcal{P}^*$; and a function $\phi:
[k] \rightarrow \{0,1\}$ indicating if the component contains a terminal.

A state is designed to efficiently represent a graph $\wt{G}$ resulting from some vertex deletions
on our input graph with just the information that is necessary for the computation. If a label is in
$P_f$, it means that we expect that its vertices will not receive additional incident edges in the
rest of the construction. Thus, a label in this situation will only be joined to a label in
$P_\varnothing$, which guarantees that no edge is added. For vertices in labels that are in the same
$P_i$, we expect that they will be in the same connected component and \emph{already} consider
them as connected. Two nonempty labels are only joined when they are in the same $P_i$, using this
we make sure that the actual connected components of $\wt{G}$ are subdivisions of the $\bigcup_{j \in
P_i} V_j(\wt{G})$ with the addition of some vertices of labels in $P_f$.

The difficult question of knowing what is accessible from a vertex is circumvented by our guessing
strategy, which takes the form of the partitioning of non empty labels into $P_f$ and
$P_1,\dots,P_k$. When a vertex is in a label of $P_f$, we know from our guess that the question is
irrelevant because we are done adding edges incident to this vertex. When a vertex is in a label of
$P_i$, we know from our guesses that it can only be connected to vertices in labels of $P_f$ and to
vertices in labels of $P_i$.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\node[circle,draw] (P0) at (0,0) {$P_\varnothing$};
		\node[circle,draw] (P1) at (2,1) {$P_1$};
		\node (dots) at (2,-0.1) {$\vdots$};
		\node[circle,draw] (Pk) at (2,-1) {$P_k$};
		\node[circle,draw] (Pf) at (4,0) {$P_f$};
		\draw (P0) edge[-stealth] (P1) edge[-stealth] (Pk) edge[out=75,in=105,-stealth] (Pf);
		\draw (Pf) edge[stealth-] (P1) edge[stealth-] (Pk);
		\draw (P0) edge[loop below,-stealth] (P0); 
		\draw (P1) edge[loop below,-stealth] (P1); 
		\draw (Pk) edge[loop below,-stealth] (Pk); 
		\draw (Pf) edge[loop below,-stealth] (Pf);
	\end{tikzpicture}
	\caption{The automaton shows how labels are allowed to move after a join.}
	\label{fig:label-automaton}
\end{figure}

The details of the algorithm are in appendix (\ref{subsec:nmwc-proof}), the correctness proofs are
structured similarly to the proofs of the algorithm of Subsection \ref{subsec:tw-algo}, to represent
the guesses on labels we use an auxiliary graph that contains the graph under construction with
additional edges to connect vertices when their labels are in the same set of $\mathcal{P}^*$.
