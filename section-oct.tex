\def\s{\mathbf{s}}
\def\u{\mathbf{u}}
\def\v{\mathbf{v}}
\def\x{\mathbf{x}}
\def\y{\mathbf{y}}

To introduce our algorithm for OCT on $k$-expressions, we first explain the idea behind the known
algorithm for tree decompositions, which is optimal under the SETH \cite{lokshtanov2011known}.

It is a dynamic programming algorithm with the following states: for each node $t$ of the nice tree
decomposition, each tripartition $(U,A,B)$ of $X_t$, compute the minimum $|\wt{U}|$ for all
tripartitions $(\wt{U},\wt{A},\wt{B})$ of $V(G_t)$ such that $U = X_t \cap \wt{U}$, $A = X_t \cap
\wt{A}$, $B = X_t \cap \wt{B}$, and $\wt{A}$ and $\wt{B}$ are independent sets of $G_t - \wt{U}$.
The intuition is that we construct a bipartition for the graph resulting from vertex deletion,
ensuring that it is bipartite, meaning it has no odd cycle. Furthermore, the part of the
tripartition of the vertices of the partial graph that is not in the current bag has no effect on
the constraints on new vertices.

We will simply adapt the same idea to $k$-expressions. As for the previous algorithms on
$k$-expressions, we have to find a way to manage the fact that labels may contain many vertices.
Luckily, in this case it will be quite simple, due to the following fact: 

\begin{fact}
	For $(A,B)$ a bipartition of graph labeled graph $H$,
	$(A,B)$ is a not a bipartition of $\eta_{i \times j}(H)$ if and only if there is a side of the
	bipartition with a vertex in each of $i$ and $j$.
\end{fact}

\begin{proof}
	Suppose that $(A,B)$ is not a bipartition of $H'=\eta_{i \times j}(H)$ then $A$ or $B$ is not an
	independent set of $H'$. Without loss of generality, we assume that $A$ is not an independent
	set of $H'$, hence there exists $u,v \in A$ adjacent in $H'$. Since $A$ is an independent set of
	$H$, edge $uv$ must be added by the join operation between $i$ and $j$, so one of $u,v$ is in
	$i$ and the other is in $j$. 

	Conversely, if there is a side of the bipartition with a vertex in each of the joined labels
	then in $H'$ there is an edge between them and this side is not an independent set anymore, so
	$(A,B)$ is not a bipartition of $H'$.
\end{proof}

This means that to check if a join $\eta_{i \times j}$ is compatible with a bipartition $(A,B)$, we
just have to know the values of $|A \cap V_i|>0$, $|A \cap V_j|>0$, $|B \cap V_i|>0$, and $|B \cap
V_j|>0$.  This requires 2 bits of information per label, hence there are $4^k$ possible
configurations.  For each node of the $k$-expression, we have a state for each of the $4^k$ possible
configurations.  Note that manipulating these informations is easy when performing a renaming label
operation or disjoint union operation.

To obtain our optimal complexity, we have to use a smart way of computing disjoint union operations.
We do it in time $\O(4^kk)$ (see Lemma \ref{lem:oct-cplx}), while the naive enumeration of pairs of
states takes time $\O(16^k)$. 

For the lower bound, we encode SAT with an OCT instance that can be constructed by a linear
$k$-expression. The main ideas are to represent SAT variables by paths, to impose that consecutive
paths are on distinct sides of the optimal bipartition, and to encode clauses by odd cycles. The reason
behind the optimal base of the exponent is that we can manage to store two variables in one label
(more precisely the current endpoints of the two consecutive paths), this will result in additional
edges between the two paths but they can be ignored because they are between vertices on distinct
sides of the optimal bipartition.

The paths representing variables come from the reduction for \textsc{Independent Set} and the
gadgets to transmit information called ``arrows'' come from the reduction for \textsc{Odd Cycle
Transversal} both in \cite{lokshtanov2011known}.

\begin{theorem}
Given a $k$-expression describing graph $G$, \OCT{} can be solved in time $\O(4^k \cdot k \cdot n)$.
Furthermore, the base of the exponent is optimal under the SETH.
\end{theorem}

Complete description and proofs are provided in the appendix (\ref{subsec:oct-proof}).
