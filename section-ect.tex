\def\sq{{\small $\square$}}

In \cite{bergougnoux2020close}, Bergougnoux, Bonnet, Brettell, and Kwon gave a $2^{\Omega(\tw \log
\tw)}n^{\O(1)}$ lower bound for \SFVS{}, \SOCT{}, and \ECT{} under the ETH. We present an
algorithm of complexity $2^{\O(\tw \log \tw)}n$ for them and \SECT, closing the gap for \ECT{} and
\SECT, and improving on the previous $2^{\O(\tw \log \tw)}n^3$ algorithm for \SFVS{} and \SOCT{} of
\cite{bergougnoux2020close}.

Rather than simply giving an algorithm for just SOCT and SECT to which we can reduce ECT and SFVS,
we also show how our method gives less involved algorithms for SFVS and ECT. In order to have a
common notation, we will call \sq-cycles the cycles that have to be hit in the problem and
\sq-cycle-free the graphs that do not contain \sq-cycles.

Before diving in the explanation of the algorithm, we will discuss how to design a bottom-up dynamic
programming algorithm for these problems on a nice tree decomposition. A natural way to proceed is
to find a criterion such that the union of two \sq-cycle-free graphs with $k$ common vertices is
also \sq-cycle-free. To check that no \sq-cycle is created by the union, for each pair of the common
vertices, we need to know what type of paths exist between them (e.g. for ECT we want to know if
they are connected by an even path and if they are connected by an odd path) in each of the two
graphs. A naive way to do this is to simply have a matrix with the answer to each of these $\O(k^2)$
questions. This would lead to an algorithm running in time $2^{\O(\tw^2)}n$. By representing the
graph with an auxiliary forest, we can encode the same information using $\O(k \log k)$ bits,
leading to a $2^{\O(\tw \log \tw)}n$ algorithm.

We begin by giving a characterisation of nontrivial 2-connected \sq-cycle-free graphs for each problem.
This implies characterisations of \sq-cycle-free graphs, because it can be decomposed in 2-connected
components and then the characterisation can be applied to each component.

\begin{lemma}\label{lem:2-cc-charac}
	Let $G$ be a nontrivial 2-connected multigraph.
\begin{enumerate}
	\item $G$ contains no $S$-cycle if and only if it contains no $S$-vertex.

	\item $G$ contains no even cycle if and only if it is an odd cycle.

	\item $G$ contains no odd $S$-cycle if and only if it has one of the following forms:
	\begin{itemize}
		\item $G$ contains no $S$-vertex and is not bipartite
		\item $G$ contains no $S$-vertex and is bipartite
		\item $G$ contains at least one $S$-vertex and is bipartite.
	\end{itemize}

	\item $G$ contains no even $S$-cycle if and only if it has of one of the following forms:
	\begin{itemize}
		\item $G$ contains no $S$-vertex and is not bipartite
		\item $G$ contains no $S$-vertex and is bipartite
		\item $G$ contains at least one $S$-vertex, the connected components of $G-S$ are bipartite, together with
		$S$-vertices they form a cycle: each $S$-vertex has degree 2 and each connected component of
		$G-S$ has outdegree 2. One $S$-cycle is odd.
		We later call \emph{bipartite subcomponents} the connected components of $G-S$.
		This is illustrated in Figure \ref{fig_forest_a}.
	\end{itemize}
\end{enumerate}
\end{lemma}

The first point is immediate, the second was observed in \cite{misra2012parameterized} and the third
in \cite{bergougnoux2020close}. The last point was not known to us and we provide a proof in
appendix (\ref{subsec:proof-ect}).

\begin{defi}\label{def:underlying-forest}
Given a \sq-cycle-free graph $G$, we define its \emph{underlying forest} $F(G)$ as the graph
obtained from $G$ by modifying independently each nontrivial 2-connected component $C$ as follows:
\begin{enumerate}
	\item For SFVS, remove edges inside $C$ and add an unlabeled vertex adjacent to all
	vertices of $C$.
	\item For ECT, remove edges inside $C$ and add a vertex adjacent to all vertices of
	$C$ and label it “odd cycle”.
	\item For SOCT, remove edges inside $C$ and add a vertex adjacent to all vertices of $C$, label
	it “bipartite” or “not bipartite” based on the property of $C$ and make it an $S$-vertex if $C$
	contains an $S$-vertex.
	\item For SECT, in the two first forms we remove edges inside $C$ and add a vertex
	adjacent to all vertices of $C$ and label it “bipartite” or “not bipartite” based on
	the property of $C$. For the last form, for each bipartite subcomponent in the cycle, we
	remove its edges, add a vertex labeled “internal bipartite” adjacent to its vertices. Then
	remove edges of $C$ incident to $S$, add an $S$-vertex labeled “odd cycle” adjacent to
	$S$-vertices and vertices labeled “internal bipartite”. 
	This is illustrated in Figure \ref{fig_forest_b}.
\end{enumerate}
\end{defi}

\begin{figure}[!h]
 \begin{subfigure}{0.5\linewidth}
  \centering{\includegraphics{figure3.pdf}}
  \caption{An example of graph with no even $S$-cycle. \\ The vertices of $S$ are depicted in red. The blue \\ boxes denote the bipartite subcomponents.}
  \label{fig_forest_a}
 \end{subfigure}
 \begin{subfigure}{0.5\linewidth}
 \centering{\includegraphics{figure4.pdf}}
  \caption{The underlying forest we build from the graph on Figure \ref{fig_forest_a}. The “internal
  bipartite” vertices are depicted in blue and the “odd cycle” vertex is black.}
  \label{fig_forest_b}
 \end{subfigure}
 \caption{The last form of SECT: ``internal bipartite'' vertices}
 \label{fig_forest}
\end{figure}

Observe that, because labeled vertices are only introduced by this underlying forest, to each
labeled vertex $v$, each of them can be associated with a nontrivial 2-connected component $C$: the
one that resulted in the creation of $v$. Observe also that for a path $P$ between two unlabeled
vertices, if it contains a labeled vertex, then it contains a vertex of its associated component
before it on $P$ and another vertex of its associated component after it on $P$.

Using some reduction rules inspired by \cite{bergougnoux2020close}, the forest can be reduced to
$\mathcal{O}(\tw)$ vertices, we denote the set of reduced underlying forests with active vertices
(vertices in common when performing unions) $X$ by $F(G,X)$. The idea of the reduced forest is
to preserve only the paths between active vertices and to contract these paths as much as possible.
The length of paths still has to be maintained, there are two ways of doing this. The simplest to
describe is to have edges with weights in $\mathbb{F}_2$, but this can also be done by coloring
active vertices, which can give better constants for the size of the reduced forests.

The main technical engine of our algorithms is the following join operation (see \ref{proof:merge-F}
for proof).
\begin{lemma}\label{lem:merge-F}
There exists a polynomial-time algorithm that, for every
pair of \sq-cycle-free graphs $G_1$ and $G_2$ with $V(G_1) \cap V(G_2) = X$, 
given on input two reduced forests with valid $\mathbb{F}_2$-labelings $(F_1,\alpha_1)$
and $(F_2,\alpha_2)$, with $F_1 \in F(G_1,X)$ and $F_2 \in F(G_2,X)$, decides whether $G_1 \cup G_2$
is \sq-cycle-free and, in case of a positive answer, computes a reduced forest $F \in F(G_1 \cup G_2, X)$
and, except for the \textsc{SFVS} problem, a valid $\mathbb{F}_2$-labeling $\alpha$.
\end{lemma}

We now describe our dynamic programming algorithm on a nice tree decomposition $(T,\{X_t\}_{t \in
V(T)})$ of graph $G$. It consists of a bottom-up computation with states $(t,Y,F,\alpha)$ where $t
\in V(T)$, $Y\subseteq X_t$ is the set of undeleted vertices of $X_t$, $F$ is a labeled forest
description with active vertices $Y$, and $\alpha$ is a $\mathbb{F}_2$-labeling of edges of $F$. We
denote by $d[t,Y,F,\alpha]$ the cell of the table corresponding to this state. We call a state
\emph{reachable} if its cell is updated at least once by a transition.

We call a state $(t,Y,F,\alpha)$ \emph{admissible} if there exists $U \subseteq V(G_t)$ such that
$Y=X_t \setminus U$, $G_t-U$ is \sq-cycle-free, $F$ is a forest description of a member of
$F(G_t-U,Y)$, $\alpha$ is a valid 2-labeling of $F$ and $d[t,Y,F,\alpha]=c(U)$.

To prove the correctness of the algorithm we will prove that reachable states are admissible and
that for each $t \in V(T)$ and $U \subseteq V(G_t)$, if $G_t-U$ is \sq-cycle-free there exists a
state with value at most $c(U)$. The optimal transversal weight will be in
$d[r,\varnothing,\varnothing,\varnothing]$ where $r$ is the root of the decomposition.

We now describe the computations for each node $t$ of the nice tree decomposition based on its type.
\begin{enumerate}
	\item \textbf{Leaf node.} We set $d[t,\varnothing,\varnothing,\varnothing]=0$

	\item \textbf{Introduce vertex node.} Let $t'$ denote the child node of $t$ and $v$ be the
	introduced vertex. 
	For each reachable state $(t',Y',F',\alpha')$, we have two transitions representing the choice
	of deleting the vertex or not: $$d[t,Y',F',\alpha'] \leftarrow d[t',Y',F',\alpha'] + c(v)$$
	$$d[t',Y'\cup\{v\},F,\alpha'] \leftarrow d[t',Y',F',\alpha']$$ where $F$ is
	obtained from $F'$ by adding an isolated active vertex $v$.

	\item \textbf{Forget vertex node.} Let $t'$ denote the child node of $t$ and $v$ be the
	forgotten vertex. For each reachable state $(t',Y',F',\alpha')$, if $v \notin Y'$ then the
	transition is simply: $$d[t,Y',F',\alpha'] \leftarrow d[t',Y',F',\alpha']$$ 
	If $v \in Y'$, we perform a join processing of $(F',\alpha')$ and $(H,\beta)$ where $H$ is the
	union of a star graph with internal vertex $v$ and leaves $N_G(v) \cap Y'$ and isolated vertices
	for $Y'\setminus N_G[v]$, and $\beta$ its edges to $1$. 
	If the join processing does not reject and returns $(\widetilde{F},\widetilde{\alpha})$, we
	obtain $(F,\alpha)$ from $(\widetilde{F},\widetilde{\alpha})$ by making $v$ inactive and
	applying reduction rules, then have transition: $$d[t,Y' \setminus \{v\},F,\alpha] \leftarrow
	d[t',Y',F',\alpha']$$

	\item \textbf{Join node.} Let $t_1$ and $t_2$ denote the two children of $t$. For each pair of
	reachable states $(t_1,Y,F_1,\alpha_1)$ and $(t_2,Y,F_2,\alpha_2)$, we perform a join process of
	$(F_1,\alpha_1)$ and $(F_2,\alpha_2)$. If the join isnt rejected, we obtain $(F,\alpha)$ and
	have transition $d[t,Y,F,\alpha] \leftarrow d[t_1,Y,F_1,\alpha_1] + d[t_2,Y,F_2,\alpha_2]$.
\end{enumerate}

\begin{lemma}\label{lem:ect-sound}
	All reachable states are admissible.
\end{lemma}


\begin{lemma}\label{lem:ect-complete}
	For every node $t \in V(T)$, every $U \subseteq V(G_t)$, if $G_t-U$ is \sq-cycle-free then there
	exists $F,\alpha$ such that $F$ is a forest description of a member of $F(G_t - U,X_t \setminus
	U)$, $\alpha$ is a valid $\mathbb{F}_2$-labeling of $F$, $(t,X_t \setminus U,F,\alpha)$ is
	reachable, and $d[t,X_t \setminus U,F,\alpha] \leq c(U)$. 
\end{lemma}


\begin{lemma}\label{lem:ect-val}
	The final value of $d[r,\varnothing,\varnothing,\varnothing]$ is the weight of an optimal
	transversal.
\end{lemma}

\begin{proof}
	By applying Lemma \ref{lem:ect-complete} with $U$ a \sq-cycle transversal, because $V(G)$ is
	always a transversal, we conclude that state $(r,\varnothing,\varnothing,\varnothing)$ is
	reachable, $d[r,\varnothing,\varnothing,\varnothing] \leq c(U)$.
	By applying Lemma \ref{lem:ect-sound} to reachable state
	$(r,\varnothing,\varnothing,\varnothing)$, there exists $U^*$ a \sq-cycle transversal
	such that $d[r,\varnothing,\varnothing,\varnothing]=c(U^*)$.
	It has optimal weight by Lemma \ref{lem:ect-complete}.
\end{proof}

\begin{theorem}
	\textsc{Subset Feedback Vertex Set}, \textsc{Subset Odd Cycle Transversal}, and \textsc{Subset
	Even Cycle Transversal}, even in the weighted setting, can be solved in time $2^{\O(k \log k)}
	\cdot n$ on $n$-vertex graphs of treewidth $k$.
\end{theorem}

\begin{proof}
	We use an approximation algorithm to compute a tree decomposition of width $\O(k)$ in time
	$2^{\O(k)} \cdot n$.
	We have $2^{\O(k \log k)}\cdot n$ states and transitions. Since transitions are computed in time
	$k^{\mathcal{O}(1)}$, the values of all states are computed in time $2^{\O(k \log k)}\cdot n$.
	The solution to the problem instance is correctly computed, by Lemma \ref{lem:ect-val}.
\end{proof}

