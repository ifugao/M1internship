Our algorithm for SFVS on $k$-expressions will reuse ideas from the two previous subsections.
We will make guesses to simplify the information to be kept on our labels as for \textsc{NMwC}.
The information about how vertices are connected is more complicated as we have to consider
the fact that paths may be $S$-paths. This is done by maintaining a partially labelled forest as we
did in the algorithm on tree decompositions.

The first step to design this algorithm has been to consider the different cases of joins that can
create an $S$-cycle, see Figure \ref{figjoin}. There are few cases where the join does not create an
$S$-cycle which motivates a distinction between a finite number of what we call \emph{label states},
that allow a classification of the possible joins based on internal structure of the label.

\begin{figure}[h]
 \centerline{
 \begin{subfigure}{0.31\linewidth}
  \centering{\includegraphics{figure2a.pdf}}
  \caption{Case 1: $|A|\geq 2$, $|B|\geq 2$,\\ and $|A\cap S|+|B\cap S|\geq 1$.}
  \label{fig_join_a}
 \end{subfigure}
 \begin{subfigure}{0.41\linewidth}
  \centering{\includegraphics{figure2b.pdf}}
  \caption{Case 2: $|A|\geq 1$, $|B|\geq 2$, and there\\ is an $S$-path with endpoints in $B$.}
  \label{fig_join_b}
 \end{subfigure}
 \begin{subfigure}{0.31\linewidth}
  \centering{\includegraphics{figure2c.pdf}}
  \caption{Case 3: $|A|\geq 1$, $|B|\geq 1$,\\ and there is an $S$-path connecting a vertex of $A$ to one of $B$.}
  \label{fig_join_c}
 \end{subfigure}}
  \caption{The three cases when a join (depicted in green) creates an $S$-cycle. The figures illustrate the smallest number of vertices of $S$ required. Thus, up to symmetry, the vertices depicted in red have to be in $S$ while the vertices in white may or may not be in $S$.}
 \label{figjoin}
\end{figure}

State $Q_\varnothing$ is assigned to labels that are completely contained in the current deletion
set, which means that the label is empty for the graph resulting from the deletion.
States $Q_1$ and $Q_1^*$ are assigned to labels consisting of a single non-$S$-vertex, or a single
$S$-vertex, respectively. 
States $Q_w$ and $Q_w^*$ are called \emph{waiting states}: they are assigned to labels for which we
have guessed that they will be joined (only once) to a non-$S$-vertex from a label in state $Q_1$,
or to an $S$-vertex from a label in state $Q_1^*$, respectively. 
State $Q_2$ is assigned to labels having at least two vertices but no $S$-vertex: it is assigned to labels
for which we have guessed that they will be joined (potentially several times) to either a vertex
from a label in state $Q_1$, or to vertices from a label in state $Q_2$.
These guessing tricks can be seen as a form of what is called ``expectation from the outside'' in
\cite{Bui-XuanSTV13}.
We point that guessing these joins implies that labels in states $Q_w,Q_w^*,Q_2$ will eventually be
connected.
At last, state $Q_f$ is called \emph{final state}: it will contain vertices that will not be joined
anymore. 
%
To summarize, states in $\mathcal{Q}$ express the following constraints on joins:
\begin{itemize}
	\item joins with a label in state $Q_\varnothing$ will be ignored; 
	\item no join with a label in state $Q_f$ will be performed;
	\item labels in state $Q_w$ (resp.~$Q_w^*$) will only be joined with those in state $Q_1$
	(resp.~$Q_1^*$); and
	\item labels in state $Q_2$ will never be joined with those in state $Q_1^*$.
\end{itemize}

As illustrated by Subfigure \ref{fig_join_c}, we need to check the properties of a potential path
between two labels. To cope with these constraints, we also give a reduced forest with a vertex per
label and contracted paths between them. We have to adapt the notion of active vertices to the
context of $k$-expressions, now they are vertices representing labels that are not in state $Q_f$.
This makes a lot of sense: in the context of the nice tree decomposition, no additional incident
edges may be added to a vertex once it has been forgotten, and here, thanks to the guessing
technique, we are exactly in the same case where we know that there will not be any additional edge.

\begin{theorem}\label{thm:sfvs-complexity}
	Given a $k$-expression describing graph $G$, \SFVS{} can be solved in time $2^{\mathcal{O}(k
	\log k)} \cdot n$, where $n$ is the size of the given $k$-expression.
\end{theorem}

The details of the computation and the proof of the algorithm are deferred to the appendix
(\ref{subsec:sfvs-proof}).
